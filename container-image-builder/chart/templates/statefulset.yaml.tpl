apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: builder
spec:
  selector:
    matchLabels:
      app: builder
  podManagementPolicy: Parallel
  replicas: {{ .Values.replicas }}
  serviceName: builder
  template:
    metadata:
      labels:
        app: builder
    spec:
      containers:
        - name: builder
          image: utopiaplanitia/docker-image-builder-worker:1.6.0@sha256:7d761fc3f67d2bcf74db96e994c411dd57ae4eb22d84e280c05d25a1a2802b62
          args:
            - --docker=http://127.0.0.1:2376
          env:
            - name: DOCKER_HOST
              value: tcp://127.0.0.1:2376
            - name: DOCKER_API_VERSION
              value: "1.35"
          ports:
            - containerPort: 2375
              name: docker
          livenessProbe:
            tcpSocket:
              port: 2375
          readinessProbe:
            httpGet:
              path: /healthz
              port: 2375
          volumeMounts:
            - mountPath: /var/lib/docker
              name: docker
          resources:
            requests:
              memory: 30Mi
              cpu: 30m
            limits:
              memory: 30Mi
              cpu: 30m
        - name: docker
          image: docker:20.10.12-dind@sha256:6f2ae4a5fd85ccf85cdd829057a34ace894d25d544e5e4d9f2e7109297fedf8d
          args:
            - dockerd
            - --host=tcp://127.0.0.1:2376
            - --insecure-registry=10.0.0.0/8
            - --insecure-registry=172.16.0.0/12
            - --insecure-registry=192.168.0.0/16
            - --registry-mirror=http://mirror:5000
            - --mtu=1400
          env:
            - name: DOCKER_DRIVER
              value: overlay2
            - name: DOCKER_HOST
              value: tcp://127.0.0.1:2376
          securityContext:
            privileged: true
          livenessProbe:
            exec:
              command:
                - docker
                - version
          readinessProbe:
            httpGet:
              path: /_ping
              port: 2375
          resources:
            requests:
              memory: 7000Mi
              cpu: 100m
            limits:
              memory: 7000Mi
          volumeMounts:
            - mountPath: /var/lib/docker
              name: docker
      volumes:
        - name: docker
          emptyDir: {}
